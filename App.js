import 'react-native-gesture-handler';
import React from "react";
import { View } from "react-native";
import Routes from "./src/Navigation/Routes";



function App () {
  return (
    <View style={{flex : 1,}}>
       <Routes/>
    </View>
  )
}

export default App;