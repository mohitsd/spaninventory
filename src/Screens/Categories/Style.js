import { StyleSheet } from "react-native"

const Styles = StyleSheet.create({
    mainContainer : {
        paddingHorizontal:20,
    },
    titleName : {
        fontSize:24,
        fontWeight: '500',
        color : '#30304C',
        paddingTop:10,
        paddingBottom:15
    },
    categoryContainer : {
        paddingHorizontal : 15
    },
    catBox : {

        backgroundColor : '#EDF7FF',
        marginVertical: 10,
        paddingHorizontal:15,
        paddingVertical:15
    },
    catTitleImageBox : {
        flex:1,
        flexDirection:'row',
    },
    catName : {
        fontSize:18,
        color : '#30304C',
        flex : 1
    },
    catImage : {
        width:25, height:25
    },
    productCount : {
        color : '#616161'
    }
})

export default Styles