import React from "react";
import {View, Text, Image , FlatList, TouchableOpacity } from "react-native";
import HeaderComponent from "../../Components/HeaderComponent";
import Styles from "./Style";
import ImagePath from "../../Constants/ImagePath";

const Categories = (props) => {

    const categoriesName = [
        {
            name : 'Laminate Floors',
            totalProducts : 20,
        },
        {
            name : 'Engineered Floors',
            totalProducts : 83,
        },
        {
            name : 'Solidwood Floors',
            totalProducts : 24,
        },
        {
            name : 'Creative Collection',
            totalProducts : 35,
        }
    ]

    return (
        <View style={{backgroundColor:'#fff', flex:1}}>
            <HeaderComponent goBack
            //   goBack={() => props.navigation.goBack()}
            title="Categories" />

        <View style={Styles.categoryContainer}>
             <Text style={Styles.titleName}> {props.route.name} </Text>

            <FlatList showsHorizontalScrollIndicator={false} keyExtractor={(friend) => friend.name } data= {categoriesName} renderItem={(data) => {
                    return (
                        <View style={Styles.catBox}>
                          <View style={Styles.catTitleImageBox}>
                            <Text style={Styles.catName}> {data.item.name} </Text>
                            <Image style={Styles.catImage} source={ImagePath.rightArrow} />
                           </View>

                           <View>
                               <Text style={Styles.productCount}> {data.item.totalProducts} Products </Text>
                           </View>
                        </View>
                    )
                }}/>
        </View>
        </View>
    )
}

export default Categories