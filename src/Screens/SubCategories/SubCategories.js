import React from "react";
import { View, Text, ScrollView, FlatList, TouchableOpacity } from "react-native";
import HeaderComponent from "../../Components/HeaderComponent";
import SearchBar from "../../Components/SearchComp";
import Styles from "../Categories/Style";
import Feather from 'react-native-vector-icons/Feather';

const SubCategories = (props) => {
console.log(props, "sub")
    const subCategories = [
        {
            name : 'Airtight Tones',
            totalProducts : 20,
        },
        {
            name : 'Medium Natural Tones',
            totalProducts : 83,
        },
        {
            name : 'Solidwood Floors',
            totalProducts : 24,
        },
        {
            name : 'Earthy Brown Tones',
            totalProducts : 35,
        },
        {
            name : 'White, Grey & Black Tones',
            totalProducts : 40,
        }
    ]

  return (
    <View  style={{backgroundColor:'#fff', flex:1}}>
      <HeaderComponent goBack title={props.route.params.catId} />

      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={Styles.categoryContainer}>

            <Text style={Styles.titleName}> {props.route.params.catId} </Text>
             <View style={{height:50, marginBottom:15}}>
             <SearchBar  />
             </View>


            <FlatList
              showsHorizontalScrollIndicator={false}
              data={subCategories}
              keyExtractor={(cat) => cat.name }
              renderItem={(data) => {
                  return (
                    <TouchableOpacity style={Styles.catBox} onPress={() =>
                        {props.navigation.navigate("SubLevelSubCategories", {catId : data.item.name })}
                      }>
                           <View style={Styles.catTitleImageBox}>
                             <Text style={Styles.catName} > {data.item.name}  </Text>
                             <Feather style={Styles.catImage}   name={'chevron-right'}  />
                            </View>

                            <View>
                                <Text style={Styles.productCount}> {data.item.totalProducts} Products </Text>
                            </View>

                        </TouchableOpacity>
                  )
              }}
            />

        </View>
      </ScrollView>
    </View>
  );
};

export default SubCategories;
