import React from "react";
import { View, Text, FlatList, ScrollView,  TouchableOpacity } from "react-native";
import HeaderComponent from "../../../Components/HeaderComponent";
import SearchBar from "../../../Components/SearchComp";
import Styles from "../../Categories/Style";
import Feather from 'react-native-vector-icons/Feather';

const SubLevelSubCategories = (props) => {
console.log(props, "sub")
    const subCategories = [
        {
            name : 'Classic Planks',
            totalProducts : 20,
        },
        {
            name : 'Fashionable Herringbones & Chevron',
            totalProducts : 83,
        },
        {
            name : 'Earthy Brown Tones',
            totalProducts : 24,
        },
        {
            name : 'White, Grey & Black Tones',
            totalProducts : 35,
        },
    ]

  return (
    <View  style={{backgroundColor:'#fff', flex:1}}>
      <HeaderComponent goBack title="Sub Level Categories" />

      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={Styles.categoryContainer}>
            <Text style={Styles.titleName}> {props.route.params.catId} </Text>
             <View style={{height:50, marginBottom:15}}>
             <SearchBar  />
             </View>


            <FlatList
              showsHorizontalScrollIndicator={false}
              data={subCategories}
              keyExtractor={(cat) => cat.name }
              renderItem={(data) => {
                  return (
                    <TouchableOpacity style={Styles.catBox}  onPress={() =>
                      {props.navigation.navigate("ProductList", {catId : data.item.name })}
                    }>
                           <View style={Styles.catTitleImageBox}>
                             <Text style={Styles.catName} >{data.item.name}  </Text>
                             <Feather style={Styles.catImage}   name={'chevron-right'}  />
                            </View>

                            <View>
                                <Text style={Styles.productCount}>{data.item.totalProducts} Products </Text>
                            </View>

                        </TouchableOpacity>
                  )
              }}
            />

        </View>
      </ScrollView>
    </View>
  );
};

export default SubLevelSubCategories;
