export { default as Home } from "./Home/Home";
export { default as Categories } from "./Categories/Categories";
export {default as Search} from "./Search/Search";