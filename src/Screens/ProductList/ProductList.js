import React from "react";
import { View, Text, FlatList, Image, ScrollView,  TouchableOpacity } from "react-native";
import HeaderComponent from "../../Components/HeaderComponent";
import SearchBar from "../../Components/SearchComp";
import Styles from "../Categories/Style";
import Feather from 'react-native-vector-icons/Feather';
import ImagePath from "../../Constants/ImagePath";


const ProductList = (props) => {

    const productList = [
        {
            name : '34077 Hickory Kansas',
            totalProducts : '1000 sq feet',
            image : ImagePath.product1
        },
        {
            name : '34480 Hickory Kansas',
            totalProducts : '2200 sq feet',
            image : ImagePath.product2
        },
        {
            name : 'Oak Fresco Lodge',
            totalProducts : '2450 sq feet',
            image : ImagePath.product3
        },
        {
            name : 'R-471 Antartica Sapphire',
            totalProducts : '6000 sq feet',
            image : ImagePath.product4
        },
    ]

    return (
        <View  style={{backgroundColor:'#fff', flex:1}}>
             <HeaderComponent goBack title="Product List" />

             <ScrollView showsVerticalScrollIndicator={false}>
             <View style={Styles.categoryContainer}>
            <Text style={Styles.titleName}> {props.route.params.catId} </Text>
             <View style={{height:50, marginBottom:15}}>
             <SearchBar  />
             </View>


            <FlatList
              showsHorizontalScrollIndicator={false}
              data={productList}
              keyExtractor={(cat) => cat.name }
              renderItem={(data) => {
                  return (
                    <TouchableOpacity style={Styles.catBox} onPress={() =>
                        {props.navigation.navigate("ProductDetails", {catId : data.item.name })}
                      }>
                           <View style={Styles.catTitleImageBox}>
                               <View style={{marginRight:10}}>
                              <Image style={{width:50, height:50}} source={data.item.image} />
                              </View>
                              <View style={{flex : 2}}>
                             <Text style={Styles.catName} > {data.item.name}  </Text>
                             <Text style={Styles.productCount}> Available : {data.item.totalProducts} Products </Text>
                             </View>
                             <Feather style={Styles.catImage}   name={'chevron-right'}  />
                            </View>

                            <View>

                            </View>

                        </TouchableOpacity>
                  )
              }}
            />

        </View>
        </ScrollView>
        </View>
    )
}


export default ProductList