import { StyleSheet } from "react-native"

const Styles = StyleSheet.create({
    mainContainer : {
        paddingHorizontal:20,
    },
    titleName : {
        fontSize:24,
        fontWeight: '500',
        color : '#30304C',
        paddingTop:10,
        paddingBottom:15
    },
    loginBtn : {
        justifyContent:'flex-start',
        alignItems:'flex-start',
        width:113
    },
    loginTextBtn : {
        backgroundColor : '#30304C',
        color:'#fff',
        paddingHorizontal : 30,
        paddingVertical : 15,
        fontSize:17,
        borderRadius:50
        // fontWeight:500,

    }
})

export default Styles