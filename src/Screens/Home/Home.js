import React, {useState} from "react";
import { View, Text, TouchableOpacity, Button , SafeAreaView, StatusBar, Image } from "react-native";
import HeaderComponent from "../../Components/HeaderComponent";
import Styles from "./Style";
import TextInputFrom from "../../Components/TextInputForm";
import ImagePath from "../../Constants/ImagePath";

const Login = (props) => {
console.log(props, "homepage")

const [isVisible, setVisible] = useState(true);
const [inputBorder, setInputBorder] = useState(false);


    return (
        <View >
            <HeaderComponent />

            <View style={Styles.mainContainer}>
                <Text style={Styles.titleName}> Login </Text>
                <TextInputFrom

                //   inputStyle={{marginBottom:moderateScale(20)}}
                  placeholder="Enter your mobile number *"
                  keyboardType="email-address"
                  src={ImagePath.phoneUnvisible}
                  /* focusBorderInput={() => setInputBorder(!inputBorder)}
                  focusBorderColor={inputBorder ? colorStyle.lightGrey : colorStyle.purple} */
                />
                <TextInputFrom
                //  inputStyle={{marginBottom:moderateScale(20)}}
                 placeholder="Password"
                 secureTextEntry={isVisible}
                 src={isVisible ? ImagePath.hideEye : ImagePath.showEye}
                 onClickIcon={() => setVisible(!isVisible)}
                />

                <TouchableOpacity style={Styles.loginBtn} onPress={() => props.navigation.navigate('Categories')}>
                    <Text style={Styles.loginTextBtn}> Login </Text>
                </TouchableOpacity>
            </View>

            </View>

    )
}

export default Login