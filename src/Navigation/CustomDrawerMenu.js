import React from "react";
import {Image} from "react-native";
import ImagePath from "../Constants/ImagePath";

import {
    DrawerContentScrollView,
    DrawerItemList,
    DrawerItem
  } from '@react-navigation/drawer';

  function CustomDrawerContent(props) {
    return (
      <DrawerContentScrollView {...props}>
        <DrawerItem  label="Home"
            icon = {() => <Image style={{width:22, height:22,}} source={ImagePath.homeIcon}/>}
        />

      </DrawerContentScrollView>

    );
  }

  export default CustomDrawerContent