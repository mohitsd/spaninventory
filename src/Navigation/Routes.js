import React from "react";
import {
  NavigationContainer,
  getFocusedRouteNameFromRoute,
} from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import NavigationStrings from "../Constants/NavigationStrings";
import { createDrawerNavigator } from "@react-navigation/drawer";
import BottomTab from "./BottomTab";
import CustomDrawerContent from "./CustomDrawerMenu";

const Stack = createNativeStackNavigator();
const Drawer = createDrawerNavigator();

const Routes = () => {
  return (
    <NavigationContainer>
      <Drawer.Navigator
        initialRouteName={NavigationStrings.HOME}
        screenOptions={{
          presentation: "modal",
          headerMode: "screen",
          headerShown: false,
        }}
        drawerContent={(props) => <CustomDrawerContent {...props} />}
      >
        <Drawer.Screen
          name={NavigationStrings.BOTTOMTAB}
          component={BottomTab}
        />
        {/*   <Drawer.Screen name={NavigationStrings.HOME} component={Home} />
                <Drawer.Screen name={NavigationStrings.CATEGORIES} component={Categories} />
                <Drawer.Screen name={NavigationStrings.SEARCH} component={Search} /> */}
      </Drawer.Navigator>
    </NavigationContainer>
  );
};

export default Routes;
