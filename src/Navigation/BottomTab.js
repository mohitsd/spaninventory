import React from "react";
import { Image } from "react-native";
import { getFocusedRouteNameFromRoute } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import NavigationStrings from "../Constants/NavigationStrings";
import ImagePath from "../Constants/ImagePath";

import {
    Home,
    Categories,
    Search
} from "../Screens/Index";

  const Tab = createBottomTabNavigator();

 const BottomTab = (props) => {

  const getRouteName = () => {
    const routeName = getFocusedRouteNameFromRoute(props.route);
    console.log(routeName, "tab")
    if(routeName == 'Home' || routeName == undefined ){
      return 'none'
    }
      return 'flex'
  }
console.log(getRouteName(), "getRouteName")

    return (

        <Tab.Navigator
         screenOptions={{
            headerShown : false,
            tabBarActiveTintColor : '#B8151A',
            tabBarInactiveTintColor : '#B9BCBE'
            }}
        >
          <Tab.Screen name={NavigationStrings.HOME} component={Home}
              options={
                    () => ({
                      tabBarStyle: { display : getRouteName() },
                      tabBarIcon : ({focused}) => {
                        return (
                          <Image style={{width:22, height:22, tintColor : focused ? '#B8151A' : '#B9BCBE'}} source={ImagePath.homeIcon}/>
                        )
                      }
                })
              }

           /*  options={{
            tabBarStyle: {  },
            tabBarIcon : ({focused}) => {
              return (
                <Image style={{width:22, height:22, tintColor : focused ? '#B8151A' : '#B9BCBE'}} source={ImagePath.homeIcon}/>
              )
            }
              }} */
          />

          <Tab.Screen name={NavigationStrings.CATEGORIES} component={Categories}

              options={
                () => ({
                  tabBarStyle: { display : getRouteName() },
                  tabBarIcon : ({focused}) => {
                    return (
                      <Image style={{width:22, height:22, tintColor : focused ? '#B8151A' : '#B9BCBE'}} source={ImagePath.categoryIcon}/>
                    )
                  }
              })
              }
          />

          <Tab.Screen name="Search" component={Search}
                           options={
                            () => ({
                              tabBarStyle: { display : getRouteName() },
                              tabBarIcon : ({focused}) => {
                                return (
                                  <Image style={{width:22, height:22, tintColor : focused ? '#B8151A' : '#B9BCBE'}} source={ImagePath.searchIcon}/>
                                )
                              }
                          })
                          }
          />
        </Tab.Navigator>

    );
  }

  export default BottomTab;