export default {
    HOME : 'Home',
    CATEGORIES : 'Categories',
    SEARCH : 'Search',
    BOTTOMTAB: 'BottomTab',
    DRAWERMENU : 'DrawerMenu',
}