export default {
    hideEye : require('../../assets/images/hide-eye.png'),
    showEye : require('../../assets/images/show-eye.png'),
    phoneUnvisible : require('../../assets/images/phn-light.png'),
    phoneVisible : require('../../assets/images/phn-dark.png'),
    backArrow : require('../../assets/images/back-arrow.png'),
    homeIcon : require('../../assets/images/home-icon.png'),
    searchIcon : require('../../assets/images/search-icon.png'),
    categoryIcon : require('../../assets/images/category-icon.png'),
    menuIcon : require('../../assets/images/menu.png'),
    rightArrow : require('../../assets/images/right-arrow.png'),
}
