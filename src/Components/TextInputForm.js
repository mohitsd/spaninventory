import React from "react";
import {Text, View, TextInput, StyleSheet, Image, TouchableOpacity} from "react-native";


const TextInputFrom = (props) => {
console.log("biasdf", props.focusBorderColor)
    return (
        <View style={Styles.textInputContainer} >
            <TextInput
            style={Styles.textInput}

             placeholder={props.placeholder}
             {...props}

            />
            <TouchableOpacity style={Styles.icon} onPress={props.onClickIcon}>
              <Image style={Styles.imgStyle} source={props.src}/>
            </TouchableOpacity>
        </View>
    )
}

const Styles = StyleSheet.create({
    textInputContainer : {
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center',
        borderWidth: 1,
        height: 50,
        marginBottom:20,
        borderColor : '#979797',
        paddingHorizontal : 25,
        borderRadius:50,
        color: '#979797'
    },
    textInput : {
        height: 50,
        width: '100%'
    },
    imgStyle : {
        width :18,
        height: 18,
        position:'relative',
        right : 15
    },
    icon : {
        height:50,
        justifyContent:'center'
    }
})

export default TextInputFrom;