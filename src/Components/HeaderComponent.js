import React from "react";
import {useNavigation} from "@react-navigation/native";
import {View, Text, StyleSheet, TouchableOpacity, ImageBackground, Image} from "react-native";
import { scale, verticalScale, moderateScale } from 'react-native-size-matters';
import ImagePath from "../Constants/ImagePath";

const LoginHeader = ({goBack}) => {
    console.log(goBack, "headerFile")
    const navigation = useNavigation();

    return (
        <View style={Styles.container}>
            <ImageBackground style={Styles.imgStyle}
            resizeMode="cover"
            source={require('../../assets/images/header.png')}
           >
               {/* <View style={{flex:1, paddingLeft:15}}>
                {!!goBack ? <TouchableOpacity
                              onPress={ () => {navigation.goBack()}}
                              >
                                    <Image style={Styles.arrowStyle} source={ImagePath.backArrow}/>
                            </TouchableOpacity> : <Text />}
               </View> */}
               <View style={{flex:1, paddingLeft:15}}>
                {!!goBack ? <TouchableOpacity
                              onPress={ () => {navigation.openDrawer()}}
                              >
                                    <Image style={Styles.arrowStyle} source={ImagePath.menuIcon}/>
                            </TouchableOpacity> : <Text />}
               </View>

                <View style={{alignItems:'center', flex:10}}>
                  <Text style={Styles.textTitleStyle}> SPAN Floor </Text>
                  <Text style={Styles.numberStyle}> +91-9971509922, +91-9212096387 </Text>
                </View>

                <View style={{flex:1, paddingRight:15}}>
                    <Text></Text>
                </View>

                </ImageBackground>
        </View>
    )
}

const Styles = StyleSheet.create({
    container : {

    },
    imgStyle : {
        flexDirection:'row',
        justifyContent:'space-around',
        height: verticalScale(117),
        width: '100%',
        alignItems:'center',
        paddingTop:25,
    },
    arrowStyle: {
        width:25, height:25
    },
    textTitleStyle : {
        fontSize: 22,
        fontWeight : 'bold',
        color : '#EEEEEF',
    },
    numberStyle : {
        fontSize: 14,
        fontWeight : 'bold',
        color : '#EEEEEF',
    }
})

export default LoginHeader;